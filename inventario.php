<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <title>Inventario Hosts</title>
        <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/> 
        <!--<script src="../jquery/jquery-3.6.0.min.js" type="text/javascript"></script>
        <script src="../js/filtra_querys.js" type="text/javascript"></script>
        <script src="../bootstrap/js/sidebars.js" type="text/javascript"></script>-->
        <script src="../bootstrap/js/bootstrap.bundle.js" type="text/javascript"></script>
        <style>
          .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
          }

          @media (min-width: 768px) {
            .bd-placeholder-img-lg {
              font-size: 3.5rem;
            }
          }
          .input-group-text {background: #18277e; color: #ffffff;}
          #global {
            height: 75%;
            width: auto;
            overflow-y: scroll;
            overflow-x: scroll;
            }
            th
            {
                font-size: 13px;
                border: 1px solid black;
                text-align: center;
            }
            td
            {
                text-align: center;
                font-size: 12px;
            }
        </style>
        <link href="../bootstrap/css/sidebars.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <?php 
            include('validaUsuario.php');
            $nombre_usu = $row['nombre_usu'];
            $id_perfil = $row['id_perfil'];
            $usu_univ = $row['usu_univ'];
            $dnames=array('sist_op','cluster','proyecto');
        ?>  
        <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
            <symbol id="home" viewBox="0 0 16 16">
                <path d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146zM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5z"/>
            </symbol>
            <symbol id="gear-fill" viewBox="0 0 16 16">
                <path d="M9.405 1.05c-.413-1.4-2.397-1.4-2.81 0l-.1.34a1.464 1.464 0 0 1-2.105.872l-.31-.17c-1.283-.698-2.686.705-1.987 1.987l.169.311c.446.82.023 1.841-.872 2.105l-.34.1c-1.4.413-1.4 2.397 0 2.81l.34.1a1.464 1.464 0 0 1 .872 2.105l-.17.31c-.698 1.283.705 2.686 1.987 1.987l.311-.169a1.464 1.464 0 0 1 2.105.872l.1.34c.413 1.4 2.397 1.4 2.81 0l.1-.34a1.464 1.464 0 0 1 2.105-.872l.31.17c1.283.698 2.686-.705 1.987-1.987l-.169-.311a1.464 1.464 0 0 1 .872-2.105l.34-.1c1.4-.413 1.4-2.397 0-2.81l-.34-.1a1.464 1.464 0 0 1-.872-2.105l.17-.31c.698-1.283-.705-2.686-1.987-1.987l-.311.169a1.464 1.464 0 0 1-2.105-.872l-.1-.34zM8 10.93a2.929 2.929 0 1 1 0-5.86 2.929 2.929 0 0 1 0 5.858z"/>
            </symbol>
            <symbol id="people-circle" viewBox="0 0 16 16">
                <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
                <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
            </symbol>
        </svg>
        <main>
            <div class="d-flex flex-column flex-shrink-0 p-3 bg-light" style="width: 222px;" position: fixed;>
                <div class="py-1 text-center">
                    <h5>TD Infraestructura</h5>
                </div>
                <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto link-dark text-decoration-none">
                    <img src="../img/logotelcel.png" alt="" width="180" height="50"/>
                </a>
                <hr>
                <ul class="nav nav-pills flex-column mb-auto">
                    <li class="nav-item">
                        <a href="principal.php" class="nav-link link-dark" title="Inicio" data-bs-toggle="tooltip" data-bs-placement="right">
                            <svg class="bi me-2" width="16" height="16"><use xlink:href="#home"/></svg>
                            Inicio
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="inventario.php" class="nav-link active" aria-current="page">
                            <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                            Inventario
                        </a>
                    </li>
                    <li>
                        <a href="consultar.php" class="nav-link link-dark" title="Consultar" data-bs-toggle="tooltip" data-bs-placement="right">
                            <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                            Consultar
                        </a>
                    </li>
                    <li>
                        <a href="captura_hosts.php" class="nav-link link-dark" title="Dar de alta host." data-bs-toggle="tooltip" data-bs-placement="right">
                            <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                            Registrar Hosts
                        </a>
                    </li>
                    <li>
                        <a href="modificar.php" class="nav-link link-dark" title="Modificar host." data-bs-toggle="tooltip" data-bs-placement="right">
                            <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                            Modificar Host
                        </a>
                    </li>
                    <?php 
                        if($id_perfil==1)
                        {
                            ?>
                            <li>
                                <a href="cargar_CSV.php" class="nav-link link-dark" title="Cargar archivo CSV." data-bs-toggle="tooltip" data-bs-placement="right">
                                    <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                                    Cargar CSV
                                </a>
                            </li>
                            <?php
                        }
                    ?>
                </ul>
                <hr>
                <div class="dropdown">
                    <a href="#" class="" id="dropdownUser2" data-bs-toggle="dropdown">
                      <svg class="bi me-2" width="16" height="16"><use xlink:href="#people-circle"/></svg>
                      <span class="text-justify"><?php echo $nombre_usu; ?></span>
                    </a>
                    <ul class="dropdown-menu text-small shadow" aria-labelledby="dropdownUser2">
                      <li><a class="dropdown-item" href="salir.php">Cerrar Sesi�n</a></li>
                    </ul>
                </div>
            </div>
            <div class="b-example-divider"></div>
            <div class="col-lg-10 col-md-8 mx-auto">
                <h3 class="fw-light" align="center">Inventario Hosts</h3><br>
                <div class="table-responsive" id="global">
                    <table class="table table-striped table-sm text-uppercase">
                        <thead>
                            <tr>
                                <th>Id HOST</th>
                                <th>Tipo</th>
                                <th>Final_Marketing</th>
                                <th>Inicio de soporte</th>
                                <th>Final de soporte</th>
                                <th>Final Soporte Hypervisor</th>
                                <th>Tipo Hardware</th>
                                <th>Serie</th>
                                <th>Site</th>
                                <th>Ubicacion</th>
                                <th>Posicion</th>
                                <th>Rack</th>
                                <th>Marca</th>
                                <th>Modelo</th>
                                <th>Hostname</th>
                                <th>IP</th>
                                <th>Memoria</th>
                                <th>CPU</th>
                                <th>Cores CPU</th>
                                <th>Cores total</th>
                                <th>Tipo procesador</th>
                                <th>Procesador</th>
                                <th>Sistema Operativo</th>
                                <th>vCenter</th>
                                <th>Cluster</th>
                                <th>Ambiente</th>
                                <th>Proyecto</th>
                                <th>Aplicacion</th>
                                <th>Herramienta Monitoreo</th>
                                <th>Comentario</th>
                                <th>VMs</th>
                                <th>Porcentaje<br>CPU</th>
                                <th>Porcentaje<br>Memoria</th>
                                <th>Fecha Insercion</th>
                                <th>Fecha actualizacion</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $sql="select id_inv_host,id_tipo_host,end_of_mark,start_of_supp,end_of_supp,fin_sop_hyp,id_tipo_hw,serie,
                                site,ubicacion,posicion,rack,id_marca,modelo,
                                hostname,ip,memoria,cpu,cores_cpu,total_cores,
                                id_tipo_proc,procesador,sist_op,vcenter,cluster,id_ambiente,proyecto,
                                aplicacion,id_herr_monit,comentario,VMs,p_cpu,p_mem,fecha_insercion,fecha_actualizacion
                                from dbo.t_inv_host";
                                $stmt = sqlsrv_query( $conn, $sql);
                                if( $stmt === false) 
                                    die( print_r( sqlsrv_errors(), true) );
                                else
                                    while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC))
                                    {
                                        echo "<tr><td>".$row['id_inv_host']."</td>";
                                        if($row['id_tipo_host']==1)
                                            echo "<td>Cloud Host</td>";
                                        if($row['id_tipo_host']==2)
                                            echo "<td>Bare Metal</td>";
                                        echo "<td>".$row['end_of_mark']."</td>
                                        <td>".$row['start_of_supp']."</td>
                                        <td>".$row['end_of_supp']."</td>
                                        <td>".$row['fin_sop_hyp']."</td>";
                                        switch($row['id_tipo_hw'])
                                        {
                                            case 1:
                                                echo "<td>Blade</td>";
                                            break;
                                            case 2:
                                                echo "<td>Server</td>";
                                            break;
                                            case 3:
                                                echo "<td>Enclosure</td>";
                                            break;
                                            default:
                                                echo "<td></td>";
                                            break;
                                        }
                                        echo "<td>".$row['serie']."</td>
                                        <td>".$row['site']."</td>
                                        <td>".$row['ubicacion']."</td>
                                        <td>".$row['posicion']."</td>
                                        <td>".$row['rack']."</td>";
                                        switch($row['id_marca'])
                                        {
                                            case 1:
                                                echo "<td>Cisco</td>";
                                            break;
                                            case 2:
                                                echo "<td>HPE</td>";
                                            break;
                                            case 3:
                                                echo "<td>Huawei</td>";
                                            break;
                                            case 4:
                                                echo "<td>Ericsson</td>";
                                            break;
                                            case 5:
                                                echo "<td>Oracle</td>";
                                            break;
                                            default:
                                                echo "<td></td>";
                                            break;
                                        }  
                                        echo "<td>".$row['modelo']."</td>
                                        <td>".$row['hostname']."</td>
                                        <td>".$row['ip']."</td>
                                        <td>".$row['memoria']."</td>
                                        <td>".$row['cpu']."</td>
                                        <td>".$row['cores_cpu']."</td>
                                        <td>".$row['total_cores']."</td>";
                                        switch($row['id_tipo_proc'])
                                        {
                                            case 1:
                                                echo "<td>Intel</td>";
                                            break;
                                            case 2:
                                                echo "<td>AMD</td>";
                                            break;
                                            case 3:
                                                echo "<td>N/A</td>";
                                            break;
                                            default:
                                                echo "<td></td>";
                                            break;
                                        }  
                                        echo "<td>".$row['procesador']."</td>
                                        <td>".$row['sist_op']."</td>
                                        <td>".$row['vcenter']."</td>
                                        <td>".$row['cluster']."</td>";
                                        switch($row['id_ambiente'])
                                        {
                                            case 1:
                                                echo "<td>Administracion</td>";
                                            break;
                                            case 2:
                                                echo "<td>Produccion</td>";
                                            break;
                                            case 3:
                                                echo "<td>Pre-Produccion</td>";
                                            break;
                                            case 4:
                                                echo "<td>Desarrollo</td>";
                                            break;
                                            case 5:
                                                echo "<td>QA</td>";
                                            break;
                                            default:
                                                echo "<td></td>";
                                            break;
                                        }
                                        echo "<td>".$row['proyecto']."</td>
                                        <td>".$row['aplicacion']."</td>";
                                        switch($row['id_herr_monit'])
                                        {
                                            case 1:
                                                echo "<td>VROPS</td>";
                                            break;
                                            case 2:
                                                echo "<td>eSight/ManageOne</td>";
                                            break;
                                            case 3:
                                                echo "<td>Nagios</td>";
                                            break;
                                            case 4:
                                                echo "<td>OpenStack</td>";
                                            break;
                                            case 5:
                                                echo "<td>Cloudforms</td>";
                                            break;
                                            case 6:
                                                echo "<td>Enterprise Manager</td>";
                                            break;
                                            case 7:
                                                echo "<td>Consola Administrativa</td>";
                                            break;
                                            case 8:
                                                echo "<td>Elastic (kibana)</td>";
                                            break;
                                            case 9:
                                                echo "<td>RHV Manager</td>";
                                            break;
                                            case 10:
                                                echo "<td>Grafana</td>";
                                            break;
                                            case 11:
                                                echo "<td>SDI Manager</td>";
                                            break;
                                            default:
                                                echo "<td></td>";
                                            break;
                                        }
                                        echo "<td>".$row['comentario']."</td>
                                        <td>".$row['VMs']."</td>
                                        <td>".$row['p_cpu']."</td>
                                        <td>".$row['p_mem']."</td>
                                        <td>".$row['fecha_insercion']."</td>
                                        <td>".$row['fecha_actualizacion']."</td></tr>";
                                    }
                                sqlsrv_free_stmt($stmt);
                            ?>
                        </tbody>
                    </table>
                </div>
                <br>
                <div class="col-lg-3 col-md-2 mx-auto">
                    <a class="btn btn-primary" href="descargar_inventario.php" role="button">Descargar CSV</a>
                </div>
            </div>
        </main>
    </body>
</html>