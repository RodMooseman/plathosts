<?php
    include ('../bd/conexion_db.php');
    $fname = $_FILES['somefile']['name'];
    $exte = explode(".",$fname);
    if(strtolower(end($exte))=="csv")
    {
        $filename = $_FILES['somefile']['tmp_name'];
        $handle =fopen($filename,"r");
        $today = date('d/m/Y');
        $flagnull=0;
        sqlsrv_begin_transaction( $conn );
        while(($dataCSV = fgetcsv($handle,5000,","))!==FALSE)
        {
            /*  
                Quitar acentos  y numeros en blanco 
                for($i=0;$i<34;$i++)
                {
                    $dataCSV[$i]=str_replace('\'','`',$dataCSV[$i]);
                    $dataCSV[$i]=str_replace('\"','``',$dataCSV[$i]);
                    if((($i>7&&$i<12)||($i>18&&$i<23))&&(!is_numeric($dataCSV[$i])))
                        $dataCSV[$i]=0;
                }
            */
            if($dataCSV[14]!=''&&$dataCSV[13]!='')
            {
                $sql="select id_inv_host from t_inv_host where ip='".$dataCSV[14]."' or hostname='".$dataCSV[13]."'";
                $stmt = sqlsrv_query( $conn, $sql);
                $row_count = sqlsrv_rows_affected( $stmt );
                if( $row_count != 0)  
                {
                    sqlsrv_rollback( $conn );
                    echo '<script>alert("Registro existente detectado ip='.$dataCSV[14].',\n hostname='.$dataCSV[13].' rollback aplicado.");
                    window.location.href="cargar_CSV.php";
                    </script>';
                    die( print_r( sqlsrv_errors(), true) );
                }
            }
            else 
            {
                if ($dataCSV[14]!='')
                {
                    $sql="select id_inv_host from t_inv_host where ip='".$dataCSV[14]."'";
                    $stmt = sqlsrv_query( $conn, $sql);
                    $row_count = sqlsrv_rows_affected( $stmt );
                    if( $row_count != 0)  
                    {
                        sqlsrv_rollback( $conn );
                        echo '<script>alert("Ip existente detectado ip='.$dataCSV[14].' , rollback aplicado.");
                        window.location.href="cargar_CSV.php";
                        </script>';
                        die( print_r( sqlsrv_errors(), true) );
                    }
                }
                if ($dataCSV[13]!='')
                {
                    $sql="select id_inv_host from t_inv_host where hostname='".$dataCSV[13]."'";
                    $stmt = sqlsrv_query( $conn, $sql);
                    $row_count = sqlsrv_rows_affected( $stmt );
                    if( $row_count != 0)  
                    {
                        sqlsrv_rollback( $conn );
                        echo '<script>alert("Hostname existente detectado hostname='.$dataCSV[13].', rollback aplicado.");
                        window.location.href="cargar_CSV.php";
                        </script>';
                        die( print_r( sqlsrv_errors(), true) );
                    }
                }
                if($dataCSV[14]==''&&$dataCSV[13]=='')
                    $flagnull=1;
            }
            switch($dataCSV[5])
            {
                case "Blade":
                    $tipoHW=1;
                break;
                case "Server":
                    $tipoHW=2;
                break;
                case "Enclosure":
                    $tipoHW=3;
                break;
                default:
                    $tipoHW=0;
                break;
            }
            switch($dataCSV[11])
            {
                case "Cisco":
                    $marca=1;
                break;
                case "HPE":
                    $marca=2;
                break;
                case "Huawei":
                    $marca=3;
                break;
                case "Ericsson":
                    $marca=4;
                break;
                case "Oracle":
                    $marca=5;
                break;
                default:
                    $marca=0;
                break;
            }
            switch($dataCSV[19])
            {
                case "Intel":
                    $procesador=1;
                break;
                case "AMD":
                    $procesador=2;
                break;
                case "NA":
                    $procesador=3;
                break;
                default:
                    $procesador=0;
                break;
            }
            switch($dataCSV[24])
            {
                case "Administracion":
                    $ambiente=1;
                break;
                case "Produccion":
                    $ambiente=2;
                break;
                case "Pre-Produccion":
                    $ambiente=3;
                break;
                case "Desarrollo":
                    $ambiente=4;
                break;
                case "QA":
                    $ambiente=5;
                break;
                default:
                    $ambiente=0;
                break;
            }
            switch($dataCSV[27])
            {
                case "VROPS":
                    $Hmon=1;
                break;
                case "eSight/ManageOne":
                    $Hmon=2;
                break;
                case "Nagios":
                    $Hmon=3;
                break;
                case "OpenStack":
                    $Hmon=4;
                break;
                case "Cloudforms":
                    $Hmon=5;
                break;
                case "Enterprise Manager":
                    $Hmon=6;
                break;
                case "Consola Administrativa":
                    $Hmon=7;
                break;
                case "Elastic (kibana)":
                    $Hmon=8;
                break;
                case "RHV Manager":
                    $Hmon=9;
                break;
                case "Grafana":
                    $Hmon=10;
                break;
                case "SDI Manager":
                    $Hmon=11;
                break;
                default:
                    $Hmon=0;
                break;
            }
            for($j=15;$j<19;$j++)
            {
                if($dataCSV[$j]=="")
                  $dataCSV[$j]=0;
            }
            $sql="insert into dbo.t_inv_host (
            id_tipo_host,id_host,end_of_mark,start_of_supp,end_of_supp, 
            fin_sop_hyp,id_tipo_hw,serie,site,ubicacion,posicion,
            rack,id_marca,modelo, hostname,ip,
            memoria,cpu,cores_cpu,total_cores,id_tipo_proc,
            procesador,sist_op,vcenter,cluster,id_ambiente,
            proyecto, aplicacion,id_herr_monit,comentario,fecha_insercion,
            fecha_actualizacion,num_carga,updt_by) VALUES (
            ".$_POST['tipoOfData'].",".$dataCSV[0].",'".$dataCSV[1]."','".$dataCSV[2]."','".$dataCSV[3]."', 
            '".$dataCSV[4]."',".$tipoHW.",'".$dataCSV[6]."','".$dataCSV[7]."','".$dataCSV[8]."','".$dataCSV[9]."',
            '".$dataCSV[10]."',".$marca.",'".$dataCSV[12]."','".$dataCSV[13]."','".$dataCSV[14]."', 
            ".$dataCSV[15].",".$dataCSV[16].",".$dataCSV[17].",".$dataCSV[18].",".$procesador.",
            '".$dataCSV[20]."','".$dataCSV[21]."','".$dataCSV[22]."','".$dataCSV[23]."',".$ambiente.",
            '".$dataCSV[25]."','".$dataCSV[26]."',".$Hmon.",'".$dataCSV[28]."','".$today."',
            '".$today."',1,'".$_POST['nombre_usu']."')";
            $stmt = sqlsrv_query( $conn, $sql);
            if( $stmt === false) 
            {
                sqlsrv_rollback( $conn );
                echo '<script>alert("Error en insercion , rollback aplicado.");
                    window.location.href="cargar_CSV.php";
                    </script>';
                die( print_r( sqlsrv_errors(), true) );
            }
        }
        sqlsrv_commit( $conn );
        echo '<script>alert("Archivo CSV cargado a la BD';
        if($flagnull==1)
            echo ' , existe un dato sin ip ni hostname.';
        echo '.");
        window.location.href="cargar_CSV.php";
        </script>';
    }
    else
      echo '<script>alert("Archivo no existente o incompatible.");
      window.location.href="cargar_CSV.php";
      </script>';
?>