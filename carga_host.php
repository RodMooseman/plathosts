<?php
    include ('../bd/conexion_db.php');
    sqlsrv_begin_transaction( $conn );
    $sql="select Top 1 id_host from t_inv_host where id_tipo_host=1 ORDER BY id_host DESC";
    $stmt = sqlsrv_query( $conn, $sql );
    $row_count = sqlsrv_rows_affected( $stmt );
    if( $row_count == 0)  
        $bigCloud['id_host']=0;   
    else
        $bigCloud = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);  
    //echo "BIgCloud:".$bigCloud['id_host']."\n";
    $sql="select Top 1 id_host from t_inv_host where id_tipo_host=2 ORDER BY id_host DESC";
    $stmt = sqlsrv_query( $conn, $sql );
    $row_count = sqlsrv_rows_affected( $stmt );
    if( $row_count == 0)  
        $bigBareM['id_host']=0; 
    else
        $bigBareM = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);
    //echo "bigBareM:".$bigBareM['id_host']."\n";
    $sql="select Top 1 id_host from t_inv_host where id_tipo_host=3 ORDER BY id_host DESC";
    $stmt = sqlsrv_query( $conn, $sql );
    $row_count = sqlsrv_rows_affected( $stmt );
    if( $row_count == 0)  
        $bigNA['id_host']=0; 
    else
        $bigNA = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);
    //echo "bigNA:".$bigNA['id_host']."\n";
    for($i=0;$i<$_POST['cantd'];$i++)
    {
        if($_POST['tipo'.$i]==1)
        {
            $bigCloud['id_host']++;
            $idHost=$bigCloud['id_host'];
        }
        if($_POST['tipo'.$i]==2)
        {
            $bigBareM['id_host']++;
            $idHost=$bigBareM['id_host'];
        }
        if($_POST['tipo'.$i]==3)
        {
            $bigNA['id_host']++;
            $idHost=$bigNA['id_host'];
        }
        if($_POST['ip'.$i]!=''&&$_POST['hostname'.$i]!='')
        {
            $sql="select id_inv_host from t_inv_host where ip='".$_POST['ip'.$i]."' or hostname='".$_POST['hostname'.$i]."'";
            $stmt = sqlsrv_query( $conn, $sql);
            $row_count = sqlsrv_rows_affected( $stmt );
            if( $row_count != 0)  
            {
                sqlsrv_rollback( $conn );
                echo '<script>alert("Registro existente detectado ip='.$_POST['ip'.$i].',\n hostname='.$_POST['hostname'.$i].' rollback aplicado.");
                    window.location.href="captura_hosts.php";
                    </script>';
                die( print_r( sqlsrv_errors(), true) );
            }
        }
        else 
        {
            if ($_POST['ip'.$i]!='')
            {
                $sql="select id_inv_host from t_inv_host where ip='".$_POST['ip'.$i]."'";
                $stmt = sqlsrv_query( $conn, $sql);
                $row_count = sqlsrv_rows_affected( $stmt );
                if( $row_count != 0)  
                {
                    sqlsrv_rollback( $conn );
                    echo '<script>alert("IP existente detectado IP='.$_POST['ip'.$i].' , rollback aplicado.");
                    window.location.href="captura_hosts.php";
                    </script>';
                    die( print_r( sqlsrv_errors(), true) );
                }
            }
            if ($_POST['hostname'.$i]!='')
            {
                $sql="select id_inv_host from t_inv_host where hostname='".$_POST['hostname'.$i]."'";
                $stmt = sqlsrv_query( $conn, $sql);
                $row_count = sqlsrv_rows_affected( $stmt );
                if( $row_count != 0)  
                {
                    sqlsrv_rollback( $conn );
                    echo '<script>alert("Hostname existente detectado hostname='.$_POST['hostname'.$i].', rollback aplicado.");
                    window.location.href="captura_hosts.php";
                    </script>';
                    die( print_r( sqlsrv_errors(), true) );
                }
            }
            if($_POST['ip'.$i]==''&&$_POST['hostname'.$i]=='')
                $flagnull=1;
        }
        $today = date('d/m/Y');
        $sql="INSERT INTO dbo.t_inv_host (id_tipo_host,id_host,end_of_mark,start_of_supp,end_of_supp,
        fin_sop_hyp,id_tipo_hw,serie,site,ubicacion,posicion,rack,id_marca,modelo,
        hostname,ip,memoria,cpu,cores_cpu,total_cores,id_tipo_proc,procesador,sist_op,vcenter,cluster,id_ambiente,proyecto,
        aplicacion,id_herr_monit,comentario,VMs,p_cpu,p_mem,fecha_insercion,fecha_actualizacion,num_carga,updt_by) VALUES 
        (".$_POST['tipo'.$i].",".$idHost.",'".$_POST['end_of_mark'.$i]."','".$_POST['start_of_supp'.$i]."','".$_POST['end_of_supp'.$i]."',
        '".$_POST['fin_sop_hyp'.$i]."',".$_POST['tipo_hw'.$i].",'".$_POST['serie'.$i]."','".$_POST['site'.$i]."','".$_POST['ubicacion'.$i]."','".$_POST['posicion'.$i]."',
        '".$_POST['rack'.$i]."',".$_POST['marca'.$i].",'".$_POST['modelo'.$i]."','".$_POST['hostname'.$i]."','".$_POST['ip'.$i]."',
        ".$_POST['memoria'.$i].",".$_POST['cpu'.$i].",".$_POST['cores_cpu'.$i].",".$_POST['total_cores'.$i].",".$_POST['tipo_proc'.$i].",'".$_POST['procesador'.$i]."',
        '".$_POST['sist_op'.$i]."','".$_POST['vcenter'.$i]."','".$_POST['cluster'.$i]."',".$_POST['ambiente'.$i].",'".$_POST['proyecto'.$i]."',
        '".$_POST['aplicacion'.$i]."',".$_POST['herr_monit'.$i].",'".$_POST['comentario'.$i]."',".$_POST['VMs'.$i].",".$_POST['p_cpu'.$i].",".$_POST['p_mem'.$i].",'".$today."','".$today."',1,'".$_POST['nombre_usu']."')";
        $stmt = sqlsrv_query( $conn, $sql);
        if( $stmt === false) 
        {
            sqlsrv_rollback( $conn );
            echo '<script>alert("Error en insercion , rollback aplicado.");
                    window.location.href="captura_hosts.php";
                    </script>';
            die( print_r( sqlsrv_errors(), true) );
        }
        /*echo $sql."\n";*/
    }
    sqlsrv_commit( $conn );
    echo '<script>alert("usuarios agregados a la BD.");
    window.location.href="principal.php";
    </script>';
?>