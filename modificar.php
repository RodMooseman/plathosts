<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <title>Inventario Hosts</title>
        <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <style>
          .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
          }

          @media (min-width: 768px) {
            .bd-placeholder-img-lg {
              font-size: 3.5rem;
            }
          }
          .input-group-text {background: #18277e; color: #ffffff;}
          #global {
            height: 66%;
            width: auto;
            overflow-y: scroll;
            overflow-x: scroll;
            }
            th
            {
                border: hidden;
                font-size: 12px;
                /*border: 1px solid black;*/
                text-align: center;
            }
            td
            {
                border: hidden;
                text-align: center;
                font-size: 11px;
            }
            table
            {
                border: hidden;
            }
        </style>
        <link href="../bootstrap/css/sidebars.css" rel="stylesheet" type="text/css"/>
        <!--<script src="../jquery/jquery-3.6.0.min.js" type="text/javascript"></script>
        <script src="../js/filtra_querys.js" type="text/javascript"></script>
        <script src="../bootstrap/js/sidebars.js" type="text/javascript"></script>-->
        <script src="../bootstrap/js/bootstrap.bundle.js" type="text/javascript"></script>
    </head>
    <body>
    <?php 
        include('validaUsuario.php');
        $nombre_usu = $row['nombre_usu'];
        $id_perfil = $row['id_perfil'];
        $usu_univ = $row['usu_univ'];
    ?>  
    <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
        <symbol id="home" viewBox="0 0 16 16">
            <path d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146zM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5z"/>
        </symbol>
        <symbol id="gear-fill" viewBox="0 0 16 16">
            <path d="M9.405 1.05c-.413-1.4-2.397-1.4-2.81 0l-.1.34a1.464 1.464 0 0 1-2.105.872l-.31-.17c-1.283-.698-2.686.705-1.987 1.987l.169.311c.446.82.023 1.841-.872 2.105l-.34.1c-1.4.413-1.4 2.397 0 2.81l.34.1a1.464 1.464 0 0 1 .872 2.105l-.17.31c-.698 1.283.705 2.686 1.987 1.987l.311-.169a1.464 1.464 0 0 1 2.105.872l.1.34c.413 1.4 2.397 1.4 2.81 0l.1-.34a1.464 1.464 0 0 1 2.105-.872l.31.17c1.283.698 2.686-.705 1.987-1.987l-.169-.311a1.464 1.464 0 0 1 .872-2.105l.34-.1c1.4-.413 1.4-2.397 0-2.81l-.34-.1a1.464 1.464 0 0 1-.872-2.105l.17-.31c.698-1.283-.705-2.686-1.987-1.987l-.311.169a1.464 1.464 0 0 1-2.105-.872l-.1-.34zM8 10.93a2.929 2.929 0 1 1 0-5.86 2.929 2.929 0 0 1 0 5.858z"/>
        </symbol>
        <symbol id="people-circle" viewBox="0 0 16 16">
            <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
            <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
        </symbol>
    </svg>
    <main>
        <div class="d-flex flex-column flex-shrink-0 p-3 bg-light" style="width: 222px;" position: fixed;>
            <div class="py-1 text-center">
              <h5>TD Infraestructura</h5>
            </div>
            <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto link-dark text-decoration-none">
                <img src="../img/logotelcel.png" alt="" width="180" height="50"/>
            </a>
            <hr>
            <ul class="nav nav-pills flex-column mb-auto">
                <li class="nav-item">
                    <a href="principal.php" class="nav-link link-dark" title="Inicio" data-bs-toggle="tooltip" data-bs-placement="right">
                        <svg class="bi me-2" width="16" height="16"><use xlink:href="#home"/></svg>
                        Inicio
                    </a>
                </li>
                <li class="nav-item">
                    <a href="inventario.php" class="nav-link link-dark" title="Inventario" data-bs-toggle="tooltip" data-bs-placement="right">
                        <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                        Inventario
                    </a>
                </li>
                <li>
                    <a href="consultar.php" class="nav-link link-dark" title="Consultar" data-bs-toggle="tooltip" data-bs-placement="right">
                        <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                        Consultar
                    </a>
                </li>
                <li>
                    <a href="captura_hosts.php" class="nav-link link-dark" title="Dar de alta host." data-bs-toggle="tooltip" data-bs-placement="right">
                        <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                        Registrar Hosts
                    </a>
                </li>
                <li>
                    <a href="modificar.php" class="nav-link active" title="Modificar host." aria-current="page">
                        <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                        Modificar Host
                    </a>
                </li>
                <?php 
                    if($id_perfil==1)
                    {
                        ?>
                        <li>
                            <a href="cargar_CSV.php" class="nav-link link-dark" title="Cargar archivo CSV." data-bs-toggle="tooltip" data-bs-placement="right">
                                <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                                Cargar CSV
                            </a>
                        </li>
                        <?php
                    }
                ?>
            </ul>
            <hr>
            <div class="dropdown">
                    <a href="#" class="" id="dropdownUser2" data-bs-toggle="dropdown">
                      <svg class="bi me-2" width="16" height="16"><use xlink:href="#people-circle"/></svg>

                      <span class="text-justify"><?php echo $nombre_usu; ?></span>
                    </a>
                    <ul class="dropdown-menu text-small shadow" aria-labelledby="dropdownUser2">
                      <li><a class="dropdown-item" href="salir.php">Cerrar Sesi�n</a></li>
                    </ul>
            </div>
        </div>
        <div class="b-example-divider"></div>
        <div class="col-lg-10 col-md-8 mx-auto">
            <form method='post' action='thisIP.php' id='some' >
                <br>    
                <h4 class="fw-light" align="center">Modificacion de host</h4>
                <table width="50%">
                    <tr width="100%">
                        <td width="40%"><h6 class="fw-light">IP o hostname de equipo a modificar:</h6></td>
                        <td width="40%"><input type="text" id="basic-url" id="data2" name="data2" required></td>
                        <td width="20%"><button type="submit" >Buscar dato</button></td>
                    </tr>
                </table>
            </form>
            <br>
            <div class="table-responsive" id="global">
                <form method="post" action="mood_host.php">
                    <table class="table table-striped table-sm text-uppercase">
                        <input type="hidden" id="nombre_usu" name="nombre_usu" value="<?php echo $nombre_usu;?>">
                        <?php
                            if(isset($_POST['data2']))
                            {
                                ?>
                                <tr>
                                    <th>Tipo</th>
                                    <th>Final_Marketing</th>
                                    <th>Inicio de soporte</th>
                                    <th>Final de soporte</th>
                                    <th>Final soporte hypervisor</th>
                                    <th>Tipo Hardware</th>
                                    <th>Serie</th>
                                    <th>Site</th>
                                </tr>
                                <?php
                                    $flag="";
                                    $sql="select id_inv_host,id_tipo_host,end_of_mark,start_of_supp,end_of_supp,fin_sop_hyp,id_tipo_hw,serie,
                                    site,ubicacion,posicion,rack,id_marca,modelo,
                                    hostname,ip,memoria,cpu,cores_cpu,total_cores,
                                    id_tipo_proc,procesador,sist_op,vcenter,cluster,id_ambiente,proyecto,
                                    aplicacion,id_herr_monit,comentario,VMs,p_cpu,p_mem
                                    from dbo.t_inv_host"; 
                                    $sql2=$sql." where ip='".$_POST['data2']."'";
                                    $stmt = sqlsrv_query( $conn, $sql2 );
                                    $row_count = sqlsrv_rows_affected( $stmt );
                                    if( $row_count != 0)  
                                    {
                                        $flag="ip";
                                        $Dinfo=sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);  
                                    }      
                                    else
                                    {
                                        $sql2=$sql." where hostname='".$_POST['data2']."'";
                                        $stmt = sqlsrv_query( $conn, $sql2);
                                        $row_count = sqlsrv_rows_affected( $stmt );
                                        if( $row_count != 0) 
                                        {
                                            $Dinfo=sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);  
                                            $flag="hostname";
                                        } 
                                    }
                                    if ($flag!="")
                                    {
                                        ?>
                                        <tr>
                                            <td>
                                                <input type="hidden" value="<?php echo $flag; ?>" name="flagsome" id="flagsome">
                                                <input type="hidden" value="<?php echo $nombre_usu; ?>" name="user" id="user">
                                                <input type="hidden" value="<?php echo $_POST['data2']; ?>" name="thisFlag" id="thisFlag">
                                                <select name="tipo" id="tipo" required>
                                                    <option value="1" <?php if($Dinfo['id_tipo_host']==1){ echo "selected";}?>>Cloud Host</option>
                                                    <option value="2" <?php if($Dinfo['id_tipo_host']==2){ echo "selected";}?>>Bare Metal</option>
                                                    <option value="3" <?php if($Dinfo['id_tipo_host']==3){ echo "selected";}?>>N/A</option>
                                                </select> 
                                            </td>
                                            <td><input id="end_of_mark" name="end_of_mark" type="text" value="<?php echo $Dinfo['end_of_mark']; ?>"></td>
                                            <td><input id="start_of_supp" name="start_of_supp" type="text" value="<?php echo $Dinfo['start_of_supp']; ?>"></td>
                                            <td><input id="end_of_supp" name="end_of_supp" type="text" value="<?php echo $Dinfo['end_of_supp']; ?>"></td>
                                            <td><input id="fin_sop_hyp" name="fin_sop_hyp" type="text" value="<?php echo $Dinfo['fin_sop_hyp']; ?>"></td>
                                            <td>
                                                <select name="tipo_hw" id="tipo_hw" >
                                                    <option value="" <?php if($Dinfo['id_tipo_hw']==""){ echo "selected";}?>></option>
                                                    <option value="1" <?php if($Dinfo['id_tipo_hw']==1){ echo "selected";}?>>Blade</option>
                                                    <option value="2" <?php if($Dinfo['id_tipo_hw']==2){ echo "selected";}?>>Server</option>
                                                    <option value="3" <?php if($Dinfo['id_tipo_hw']==3){ echo "selected";}?>>Enclosure</option>
                                                </select>
                                            </td>
                                            <td><input type="text" id="serie" name="serie" value="<?php echo $Dinfo['serie']; ?>" ></td>
                                            <td><input type="text" id="site" name="site" value="<?php echo $Dinfo['site']; ?>" ></td>
                                        </tr>
                                        <tr>
                                            <th>Ubicacion</th>
                                            <th>Posicion</th>
                                            <th>Rack</th>
                                            <th>Marca</th>
                                            <th>Modelo</th>
                                            <th>Hostname</th>
                                            <th>IP</th>
                                            <th>Memoria</th>
                                        </tr>
                                        <tr>
                                            <td><input type="text" id="ubicacion" name="ubicacion" value="<?php echo $Dinfo['ubicacion']; ?>" ></td>
                                            <td><input type="text" id="posicion" name="posicion" value="<?php echo $Dinfo['posicion']; ?>" ></td>
                                            <td><input type="text" id="rack" name="rack" value="<?php echo $Dinfo['rack']; ?>" ></td>
                                            <td>
                                                <select name="marca" id="marca" >
                                                    <option value="" <?php if($Dinfo['id_marca']==""){ echo "selected";}?> ></option>
                                                    <option value="1" <?php if($Dinfo['id_marca']==1){ echo "selected";}?> >Cisco</option>
                                                    <option value="2" <?php if($Dinfo['id_marca']==2){ echo "selected";}?> >HPE</option>
                                                    <option value="3" <?php if($Dinfo['id_marca']==3){ echo "selected";}?> >Huawei</option>
                                                    <option value="4" <?php if($Dinfo['id_marca']==4){ echo "selected";}?> >Ericsson</option>
                                                    <option value="5" <?php if($Dinfo['id_marca']==5){ echo "selected";}?> >Oracle</option>
                                                </select>
                                            </td>
                                            <td><input type="text" id="modelo" name="modelo" value="<?php echo $Dinfo['modelo']; ?>" ></td>
                                            <td><input type="text" id="hostname" name="hostname" value="<?php echo $Dinfo['hostname']; ?>" ></td>
                                            <td><input type="text" id="ip" name="ip" value="<?php echo $Dinfo['ip']; ?>" ></td>
                                            <td><input type="number" id="memoria" name="memoria" value="<?php echo $Dinfo['memoria']; ?>" ></td>
                                        </tr>
                                        <tr>
                                            <th>CPU</th>
                                            <th>Cores CPU</th>
                                            <th>Cores total</th>
                                            <th>Tipo procesador</th>
                                            <th>Procesador</th>
                                            <th>Sistema Operativo</th>
                                            <th>vCenter</th>
                                            <th>Cluster</th>
                                        </tr>
                                        <tr>
                                            <td><input type="number" id="cpu" name="cpu" value="<?php echo $Dinfo['cpu']; ?>" ></td>
                                            <td><input type="number" id="cores_cpu" name="cores_cpu" value="<?php echo $Dinfo['cores_cpu']; ?>" ></td>
                                            <td><input type="number" id="total_cores" name="total_cores" value="<?php echo $Dinfo['total_cores']; ?>" ></td>
                                            <td>
                                                <select name="tipo_proc" id="tipo_proc" >
                                                    <option value="1" <?php if($Dinfo['id_tipo_proc']==1){ echo "selected";}?> >Intel</option>
                                                    <option value="2" <?php if($Dinfo['id_tipo_proc']==2){ echo "selected";}?> >AMD</option>
                                                    <option value="3" <?php if($Dinfo['id_tipo_proc']==3){ echo "selected";}?> >N/A</option>
                                                </select>
                                            </td>
                                            <td><input type="text" id="procesador" name="procesador" value="<?php echo $Dinfo['procesador']; ?>" ></td>
                                            <td><input type="text" id="sist_op" name="sist_op" value="<?php echo $Dinfo['sist_op']; ?>" ></td>
                                            <td><input type="text" id="vcenter" name="vcenter" value="<?php echo $Dinfo['vcenter']; ?>" ></td>
                                            <td><input type="text" id="cluster" name="cluster" value="<?php echo $Dinfo['cluster']; ?>" ></td>
                                        </tr>
                                        <tr>
                                            <th>Ambiente</th>
                                            <th>Proyecto</th>
                                            <th>Aplicacion</th>
                                            <th>Herramienta Monitoreo</th>
                                            <th>Comentario</th>
                                            <th>VMs</th>
                                            <th>Porcentaje<br>CPU</th>
                                            <th>Porcentaje<br>Memoria</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select name="ambiente" id="ambiente" >
                                                    <option value="" <?php if($Dinfo['id_ambiente']==""){ echo "selected";}?> ></option>
                                                    <option value="1" <?php if($Dinfo['id_ambiente']==1){ echo "selected";}?> >Administracion</option>
                                                    <option value="2" <?php if($Dinfo['id_ambiente']==2){ echo "selected";}?> >Produccion</option>
                                                    <option value="3" <?php if($Dinfo['id_ambiente']==3){ echo "selected";}?> >Pre-Produccion</option>
                                                    <option value="4" <?php if($Dinfo['id_ambiente']==4){ echo "selected";}?> >Desarrollo</option>
                                                    <option value="5" <?php if($Dinfo['id_ambiente']==5){ echo "selected";}?> >QA</option>
                                                </select>
                                            </td>
                                            <td><input type="text" id="proyecto" name="proyecto" value="<?php echo $Dinfo['proyecto']; ?>" ></td>
                                            <td><input type="text" id="aplicacion" name="aplicacion" value="<?php echo $Dinfo['aplicacion']; ?>" ></td>
                                            <td>
                                                <select name="herr_monit" id="herr_monit" >
                                                    <option value="" <?php if($Dinfo['id_herr_monit']==""){ echo "selected";}?> ></option>
                                                    <option value="1" <?php if($Dinfo['id_herr_monit']==1){ echo "selected";}?> >VROPS</option>
                                                    <option value="2" <?php if($Dinfo['id_herr_monit']==2){ echo "selected";}?> >eSight/ManageOne</option>
                                                    <option value="3" <?php if($Dinfo['id_herr_monit']==3){ echo "selected";}?> >Nagios</option>
                                                    <option value="4" <?php if($Dinfo['id_herr_monit']==4){ echo "selected";}?> >OpenStack</option>
                                                    <option value="5" <?php if($Dinfo['id_herr_monit']==5){ echo "selected";}?> >Cloudforms</option>
                                                    <option value="6" <?php if($Dinfo['id_herr_monit']==6){ echo "selected";}?> >Enterprise Manager</option>
                                                    <option value="7" <?php if($Dinfo['id_herr_monit']==7){ echo "selected";}?> >Consola Administrativa</option>
                                                    <option value="8" <?php if($Dinfo['id_herr_monit']==8){ echo "selected";}?> >Elastic (kibana)</option>
                                                    <option value="9" <?php if($Dinfo['id_herr_monit']==9){ echo "selected";}?> >RHV Manager</option>
                                                    <option value="10" <?php if($Dinfo['id_herr_monit']==10){ echo "selected";}?> >Grafana</option>
                                                    <option value="11" <?php if($Dinfo['id_herr_monit']==11){ echo "selected";}?> >SDI Manager</option>
                                                </select>
                                            </td>
                                            <td><input type="text" id="comentario" name="comentario" value="<?php echo $Dinfo['comentario']; ?>"></td>  
                                            <td><input type="number" id="VMs" name="VMs" value="<?php echo $Dinfo['VMs']; ?>"></td> 
                                            <td><input type="number" step="0.01" id="p_cpu" name="p_cpu" value="<?php echo $Dinfo['p_cpu']; ?>"></td> 
                                            <td><input type="number" step="0.01" id="p_mem" name="p_mem" value="<?php echo $Dinfo['p_mem']; ?>"></td> 
                                        </tr>      
                                        <tr>
                                            <td colspan="8"><button type="submit" class="btn btn-primary">Modificar</button></td>
                                        </tr>
                                        <?php
                                    }
                                    else
                                    {
                                        ?>
                                        <script>alert("Registro inexistente.");</script>
                                        <?php
                                    }
                                ?>                 
                                <?php
                            }
                        ?>     
                    </table>
                    </div>
                </form>
            </div>   
        </div>
    </main>
    </body>
</html>