<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Inventario Hosts</title>
        <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="../bootstrap/js/bootstrap.bundle.js" type="text/javascript"></script>
        <style>
          .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
          }

          @media (min-width: 768px) {
            .bd-placeholder-img-lg {
              font-size: 3.5rem;
            }
          }
          .input-group-text {background: #18277e; color: #ffffff;}
          #global {
            height: 75%;
            width: auto;
            overflow-y: scroll;
            overflow-x: scroll;
            }
            th
            {
                font-size: 13px;
                border: 1px solid black;
                text-align: center;
            }
            td
            {
                text-align: center;
                font-size: 12px;
            }
            table, td, th {
                border: hidden;
                padding: 20px;
            }
        </style>
        <link href="../bootstrap/css/sidebars.css" rel="stylesheet" type="text/css"/>
        <!--<script src="../jquery/jquery-3.6.0.min.js" type="text/javascript"></script>
        <script src="../js/filtra_querys.js" type="text/javascript"></script>
        <script src="../bootstrap/js/sidebars.js" type="text/javascript"></script>-->
    </head>
    <body>
    <?php 
        include('validaUsuario.php');
        $nombre_usu = $row['nombre_usu'];
        $id_perfil = $row['id_perfil'];
        $usu_univ = $row['usu_univ'];
    ?>  
    <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
        <symbol id="home" viewBox="0 0 16 16">
            <path d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146zM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5z"/>
        </symbol>
        <symbol id="gear-fill" viewBox="0 0 16 16">
            <path d="M9.405 1.05c-.413-1.4-2.397-1.4-2.81 0l-.1.34a1.464 1.464 0 0 1-2.105.872l-.31-.17c-1.283-.698-2.686.705-1.987 1.987l.169.311c.446.82.023 1.841-.872 2.105l-.34.1c-1.4.413-1.4 2.397 0 2.81l.34.1a1.464 1.464 0 0 1 .872 2.105l-.17.31c-.698 1.283.705 2.686 1.987 1.987l.311-.169a1.464 1.464 0 0 1 2.105.872l.1.34c.413 1.4 2.397 1.4 2.81 0l.1-.34a1.464 1.464 0 0 1 2.105-.872l.31.17c1.283.698 2.686-.705 1.987-1.987l-.169-.311a1.464 1.464 0 0 1 .872-2.105l.34-.1c1.4-.413 1.4-2.397 0-2.81l-.34-.1a1.464 1.464 0 0 1-.872-2.105l.17-.31c.698-1.283-.705-2.686-1.987-1.987l-.311.169a1.464 1.464 0 0 1-2.105-.872l-.1-.34zM8 10.93a2.929 2.929 0 1 1 0-5.86 2.929 2.929 0 0 1 0 5.858z"/>
        </symbol>
        <symbol id="people-circle" viewBox="0 0 16 16">
            <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
            <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
        </symbol>
    </svg>
    <main>
        <div class="d-flex flex-column flex-shrink-0 p-3 bg-light" style="width: 222px;" position: fixed;>
            <div class="py-1 text-center">
              <h5>TD Infraestructura</h5>
            </div>
            <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto link-dark text-decoration-none">
                <img src="../img/logotelcel.png" alt="" width="180" height="50"/>
            </a>
            <hr>
            <ul class="nav nav-pills flex-column mb-auto">
                <li class="nav-item">
                    <a href="principal.php" class="nav-link link-dark" title="Inicio" data-bs-toggle="tooltip" data-bs-placement="right">
                        <svg class="bi me-2" width="16" height="16"><use xlink:href="#home"/></svg>
                        Inicio
                    </a>
                </li>
                <li class="nav-item">
                    <a href="inventario.php" class="nav-link link-dark" title="Inventario" data-bs-toggle="tooltip" data-bs-placement="right">
                        <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                        Inventario
                    </a>
                </li>
                <li>
                    <a href="consultar.php" class="nav-link link-dark" title="Consultar" data-bs-toggle="tooltip" data-bs-placement="right">
                        <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                        Consultar
                    </a>
                </li>
                <li>
                    <a href="captura_hosts.php" class="nav-link active" title="Dar de alta host." aria-current="page">
                        <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                        Registrar Hosts
                    </a>
                </li>
                <li>
                    <a href="modificar.php" class="nav-link link-dark" title="Modificar host." data-bs-toggle="tooltip" data-bs-placement="right">
                        <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                        Modificar Host
                    </a>
                </li>
                <?php 
                    if($id_perfil==1)
                    {
                        ?>
                        <li>
                            <a href="cargar_CSV.php" class="nav-link link-dark" title="Cargar archivo CSV." data-bs-toggle="tooltip" data-bs-placement="right">
                                <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                                Cargar CSV
                            </a>
                        </li>
                        <?php
                    }
                ?>
            </ul>
            <hr>
            <div class="dropdown">
                    <a href="#" class="" id="dropdownUser2" data-bs-toggle="dropdown">
                      <svg class="bi me-2" width="16" height="16"><use xlink:href="#people-circle"/></svg>

                      <span class="text-justify"><?php echo $nombre_usu; ?></span>
                    </a>
                    <ul class="dropdown-menu text-small shadow" aria-labelledby="dropdownUser2">
                      <li><a class="dropdown-item" href="salir.php">Cerrar Sesi�n</a></li>
                    </ul>
            </div>
        </div>
        <div class="b-example-divider"></div>
        <div class="col-lg-10 col-md-8 mx-auto">
            <form method='post' action='thisNumber.php' id='some' >
                <br>
                <h3 class="fw-light" align="center">Registro de host</h3><br>                       
                <table width="50%">
                    <tr width="100%">
                        <th width="50%"><h5 class="fw-light">Cantidad de host a ingresar:</h5></th>
                        <th width="20%"> 
                            <select name="cantd" id="cantd" >
                                <?php
                                    error_reporting(0);
                                    include ('..\bd\conexion_db.php');
                                    for($i=0;$i<25;$i++)
                                    {
                                        $o ="<option ";
                                        if($i==$_POST['cantd'])
                                            $o .= "selected ";
                                        $o .= "value=\"".$i."\">".$i."</option>";
                                        echo $o;
                                    }
                                ?>
                            </select> 
                        </th>
                        <th width="30%"><button type="submit" class="btn btn-primary">Cargar tabla</button></th>
                    </tr>
                </table>
            </form>
            <br>
            <div class="table-responsive" id="global">
                <form method="post" action="carga_host.php">
                    <table class="table table-striped table-sm text-uppercase">
                        <input type="hidden" id="cantd" name="cantd" value="<?php echo $_POST['cantd'];?>">
                        <input type="hidden" id="nombre_usu" name="nombre_usu" value="<?php echo $nombre_usu;?>">
                        <?php
                            if($_POST['cantd']>0)
                            {
                                ?>
                                <thead>
                                    <tr>
                                        <th>Tipo</th>
                                        <th>Final_Marketing</th>
                                        <th>Inicio de soporte</th>
                                        <th>Final de soporte</th>
                                        <th>Final soporte hypervisor</th>
                                        <th>Tipo Hardware</th>
                                        <th>Serie</th>
                                        <th>Site</th>
                                        <th>Ubicacion</th>
                                        <th>Posicion</th>
                                        <th>Rack</th>
                                        <th>Marca</th>
                                        <th>Modelo</th>
                                        <th>Hostname</th>
                                        <th>Ip</th>
                                        <th>Memoria</th>
                                        <th>CPU</th>
                                        <th>Cores CPU</th>
                                        <th>Cores total</th>
                                        <th>Tipo procesador</th>
                                        <th>Procesador</th>
                                        <th>Sistema Operativo</th>
                                        <th>vCenter</th>
                                        <th>Cluster</th>
                                        <th>Ambiente</th>
                                        <th>Proyecto</th>
                                        <th>Aplicacion</th>
                                        <th>Herramienta Monitoreo</th>
                                        <th>Comentario</th>
                                        <th>VMs</th>
                                        <th>Porcentaje<br>cpu</th>
                                        <th>Porcentaje<br>memoria</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        for($i=0;$i<$_POST['cantd'];$i++)
                                        {
                                            ?>
                                            <tr>
                                                <td>
                                                    <select name="tipo<?php echo $i;?>" id="tipo<?php echo $i;?>" required>
                                                        <option value="1">Cloud Host</option>
                                                        <option value="2">Bare Metal</option>
                                                        <option value="3">N/A</option>
                                                    </select> 
                                                </td>
                                                <td><input id="end_of_mark<?php echo $i;?>" name="end_of_mark<?php echo $i;?>" type="date"></td>
                                                <td><input id="start_of_supp<?php echo $i;?>" name="start_of_supp<?php echo $i;?>" type="date"></td>
                                                <td><input id="end_of_supp<?php echo $i;?>" name="end_of_supp<?php echo $i;?>" type="date"></td>
                                                <td><input id="fin_sop_hyp<?php echo $i;?>" name="fin_sop_hyp<?php echo $i;?>" type="date"></td>
                                                <td>
                                                    <select name="tipo_hw<?php echo $i;?>" id="tipo_hw<?php echo $i;?>" >
                                                        <option value=""></option>
                                                        <option value="1">Blade</option>
                                                        <option value="2">Server</option>
                                                        <option value="3">Enclosure</option>
                                                    </select>
                                                </td>
                                                <td><input type="text" id="serie<?php echo $i;?>" name="serie<?php echo $i;?>"></td>
                                                <td><input type="text" id="site<?php echo $i;?>" name="site<?php echo $i;?>"></td>
                                                <td><input type="text" id="ubicacion<?php echo $i;?>" name="ubicacion<?php echo $i;?>"></td>
                                                <td><input type="text" id="posicion<?php echo $i;?>" name="posicion<?php echo $i;?>"></td>
                                                <td><input type="text" id="rack<?php echo $i;?>" name="rack<?php echo $i;?>"></td>
                                                <td>
                                                    <select name="marca<?php echo $i;?>" id="marca<?php echo $i;?>" >
                                                        <option value=""></option>
                                                        <option value="1">Cisco</option>
                                                        <option value="2">HPE</option>
                                                        <option value="3">Huawei</option>
                                                        <option value="4">Ericsson</option>
                                                        <option value="5">Oracle</option>
                                                    </select>
                                                </td>
                                                <td><input type="text" id="modelo<?php echo $i;?>" name="modelo<?php echo $i;?>"></td>
                                                <td><input type="text" id="hostname<?php echo $i;?>" name="hostname<?php echo $i;?>"></td>
                                                <td><input type="text" id="ip<?php echo $i;?>" name="ip<?php echo $i;?>"></td>
                                                <td><input type="number" id="memoria<?php echo $i;?>" name="memoria<?php echo $i;?>"></td>
                                                <td><input type="number" id="cpu<?php echo $i;?>" name="cpu<?php echo $i;?>"></td>
                                                <td><input type="number" id="cores_cpu<?php echo $i;?>" name="cores_cpu<?php echo $i;?>"></td>
                                                <td><input type="number" id="total_cores<?php echo $i;?>" name="total_cores<?php echo $i;?>"></td>
                                                <td>
                                                    <select name="tipo_proc<?php echo $i;?>" id="tipo_proc<?php echo $i;?>" >
                                                        <option value="1">Intel</option>
                                                        <option value="2">AMD</option>
                                                        <option value="3">N/A</option>
                                                    </select>
                                                </td>
                                                <td><input type="text" id="procesador<?php echo $i;?>" name="procesador<?php echo $i;?>"></td>
                                                <td><input type="text" id="sist_op<?php echo $i;?>" name="sist_op<?php echo $i;?>"></td>
                                                <td><input type="text" id="vcenter<?php echo $i;?>" name="vcenter<?php echo $i;?>"></td>
                                                <td><input type="text" id="cluster<?php echo $i;?>" name="cluster<?php echo $i;?>"></td>
                                                <td>
                                                    <select name="ambiente<?php echo $i;?>" id="ambiente<?php echo $i;?>" >
                                                        <option value=""></option>
                                                        <option value="1">Administracion</option>
                                                        <option value="2">Produccion</option>
                                                        <option value="3">Pre-Produccion</option>
                                                        <option value="4">Desarrollo</option>
                                                        <option value="5">QA</option>
                                                    </select>
                                                </td>
                                                <td><input type="text" id="proyecto<?php echo $i;?>" name="proyecto<?php echo $i;?>"></td>
                                                <td><input type="text" id="aplicacion<?php echo $i;?>" name="aplicacion<?php echo $i;?>"></td>
                                                <td>
                                                    <select name="herr_monit<?php echo $i;?>" id="herr_monit<?php echo $i;?>" >
                                                        <option value=""></option>
                                                        <option value="1">VROPS</option>
                                                        <option value="2">eSight/ManageOne</option>
                                                        <option value="3">Nagios</option>
                                                        <option value="4">OpenStack</option>
                                                        <option value="5">Cloudforms</option>
                                                        <option value="6">EnterpriseManager</option>
                                                        <option value="7">Consola Administrativa</option>
                                                        <option value="8">Elastic (kibana)</option>
                                                        <option value="9">RHV Manager</option>
                                                        <option value="10">Grafana</option>
                                                        <option value="11">SDI Manager</option>
                                                    </select>
                                                </td>
                                                <td><input type="text" id="comentario<?php echo $i;?>" name="comentario<?php echo $i;?>"></td>  
                                                <td><input type="number" step="0.01" id="VMs<?php echo $i;?>" name="VMs<?php echo $i;?>"></td>  
                                                <td><input type="number" step="0.01" id="p_cpu<?php echo $i;?>" name="p_cpu<?php echo $i;?>"></td>  
                                                <td><input type="number" step="0.01" id="p_mem<?php echo $i;?>" name="p_mem<?php echo $i;?>"></td>  
                                            </tr>                       
                                            <?php
                                        }
                                    ?>
                                </tbody>
                                <?php
                            }
                        ?>
                    </table>
                    <?php 
                        if($_POST['cantd']>0)
                        {
                            ?>
                            <div class="modal-footer" style="width: 20%;">
                                <button type="submit" class="btn btn-primary">Registrar</button>
                            </div>       
                            <?php
                        }
                    ?>          
                </form>
            </div>
        </div>
    </main>
    </body>
</html>