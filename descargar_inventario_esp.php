<?php
    include('../bd/conexion_db.php');
    $archivo = fopen("../descargas/Busqueda_Hosts.csv","w");
    $delimitador = ",";
    $dnames=array('sist_op','cluster','proyecto');
    //set column headers
    $cabeceras = array('Id HOST','Tipo','Final_Marketing','Inicio de soporte','Final de soporte','Final Soporte Hipervisor','Tipo Hardware','Serie',
    'Site','Ubicacion','Posicion','Rack','Marca','Modelo',
    'Hostname','IP','Memoria','CPU','Cores CPU','Cores total',
    'Tipo procesador','Procesador','Sistema Operativo','vCenter','Cluster','Ambiente','Proyecto',
    'Aplicacion','Herramienta Monitoreo','Comentario','VMs','Porcentaje CPU','Porcentaje Memoria','Fecha Insercion','Fecha actualizacion');
    fputcsv($archivo, $cabeceras, $delimitador);
    $sql="select id_inv_host,id_tipo_host,end_of_mark,start_of_supp,end_of_supp,fin_sop_hyp,id_tipo_hw,serie,
    site,ubicacion,posicion,rack,id_marca,modelo,
    hostname,ip,memoria,cpu,cores_cpu,total_cores,
    id_tipo_proc,procesador,sist_op,vcenter,cluster,id_ambiente,proyecto,
    aplicacion,id_herr_monit,comentario,VMs,p_cpu,p_mem,fecha_insercion,fecha_actualizacion
    from dbo.t_inv_host";
    $t=1;
    for($i = 0; $i < sizeof($dnames);$i++) 
    {
        if($t==0&&$_GET[$dnames[$i]]!="")
            $sql .= " and ".$dnames[$i]."='".$_GET[$dnames[$i]]."' ";
        if($t==1&&$_GET[$dnames[$i]]!="")
        {
            $sql .= " where ".$dnames[$i]."='".$_GET[$dnames[$i]]."' ";
            $t=0;
        }
    }
    $cabeceras[0]=$sql;
    $stmt = sqlsrv_query( $conn, $sql );
    if( $stmt === false) 
    {
       die( print_r( sqlsrv_errors(), true) );
    }
    else
    {   
        while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) 
        {
            if($row['id_tipo_host']==1)
              $row['id_tipo_host']='Cloud Host';
            if($row['id_tipo_host']==2)
              $row['id_tipo_host']='Bare Metal';
            switch($row['id_tipo_hw'])
            {
                case 1:
                    $row['id_tipo_hw']='Blade';
                break;
                case 2:
                    $row['id_tipo_hw']='Server';
                break;
                case 3:
                    $row['id_tipo_hw']='Enclosure';
                break;
                default:
                    $row['id_tipo_hw']='';
                break;
            }
            switch($row['id_marca'])
            {
                case 1:
                    $row['id_marca']='Cisco';
                break;
                case 2:
                    $row['id_marca']='HPE';
                break;
                case 3:
                    $row['id_marca']='Huawei';
                break;
                case 4:
                    $row['id_marca']='Ericsson';
                break;
                case 5:
                    $row['id_marca']='Oracle';
                break;
                default:
                    $row['id_marca']='';
                break;
            }  
            switch($row['id_tipo_proc'])
            {
                case 1:
                    $row['id_tipo_proc']='Intel';
                break;
                case 2:
                    $row['id_tipo_proc']='AMD';
                break;
                case 3:
                    $row['id_tipo_proc']='N/A';
                break;
                default:
                    $row['id_tipo_proc']='';
                break;
            }
            switch($row['id_ambiente'])
            {
                case 1:
                    $row['id_ambiente']='Administracion';
                break;
                case 2:
                    $row['id_ambiente']='Produccion';
                break;
                case 3:
                    $row['id_ambiente']='Pre-Produccion';
                break;
                case 4:
                    $row['id_ambiente']='Desarrollo';
                break;
                case 5:
                    $row['id_ambiente']='QA';
                break;
                default:
                    $row['id_ambiente']='';
                break;
            }
            switch($row['id_herr_monit'])
            {
                case 1:
                    $row['id_herr_monit']='VROPS';
                break;
                case 2:
                    $row['id_herr_monit']='eSight/ManageOne';
                break;
                case 3:
                    $row['id_herr_monit']='Nagios';
                break;
                case 4:
                    $row['id_herr_monit']='OpenStack';
                break;
                case 5:
                    $row['id_herr_monit']='Cloudforms';
                break;
                case 6:
                    $row['id_herr_monit']='Enterprise Manager';
                break;
                case 7:
                    $row['id_herr_monit']='Consola Administrativa';
                break;
                case 8:
                    $row['id_herr_monit']='Elastic (kibana)';
                break;
                case 9:
                    $row['id_herr_monit']='RHV Manager';
                break;
                case 10:
                    $row['id_herr_monit']='Grafana';
                break;
                case 11:
                    $row['id_herr_monit']='SDI Manager';
                break;
                default:
                    $row['id_herr_monit']='';
                break;
            }
            fputcsv($archivo, $row);
        }
    }
    sqlsrv_free_stmt($stmt);
    fclose($archivo);
    $fileName = 'Busqueda_Hosts.csv';
    $filePath = '../descargas/Busqueda_Hosts.csv';
    if(file_exists($filePath)){
        // Define headers
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=$fileName");
        header("Content-Type: application/zip");
        header("Content-Transfer-Encoding: binary");
        // Read the file
        readfile($filePath);
        exit;
    }else{
        echo 'The file does not exist.';
    }
    header('inventario.php');
?>