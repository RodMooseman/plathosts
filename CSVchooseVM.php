<?php
    include ('../bd/conexion_db.php');
    $fname = $_FILES['somefile']['name'];
    $exte = explode(".",$fname);
    if(strtolower(end($exte))=="csv")
    {
        $array = array("VMs", "p_cpu", "p_mem");
        $filename = $_FILES['somefile']['tmp_name'];
        $handle =fopen($filename,"r");
        sqlsrv_begin_transaction( $conn );
        while(($dataCSV = fgetcsv($handle,5000,","))!==FALSE)
        {
            /*  
                Quitar acentos  y numeros en blanco 
                for($i=0;$i<34;$i++)
                {
                    $dataCSV[$i]=str_replace('\'','`',$dataCSV[$i]);
                    $dataCSV[$i]=str_replace('\"','``',$dataCSV[$i]);
                    if((($i>7&&$i<12)||($i>18&&$i<23))&&(!is_numeric($dataCSV[$i])))
                        $dataCSV[$i]=0;
                }
            */
            $flagnull=0;
            if($dataCSV[1]!='')
            {
                $sql="select id_inv_host from t_inv_host where ip='".$dataCSV[1]."'";
                $stmt = sqlsrv_query( $conn, $sql);
                $row_count = sqlsrv_rows_affected( $stmt );
                if( $row_count != -1)  
                {
                    sqlsrv_rollback( $conn );
                    echo '<script>alert("Registro inexistente detectado IP='.$dataCSV[1].'");
                    window.location.href="cargar_CSV.php";
                    </script>';
                    die( print_r( sqlsrv_errors(), true) );
                }
                $flagnull=1;
            }
            else 
            {
                if ($dataCSV[0]!='')
                {
                    $sql="select id_inv_host from t_inv_host where hostname='".$dataCSV[0]."'";
                    $stmt = sqlsrv_query( $conn, $sql);
                    $row_count = sqlsrv_rows_affected( $stmt );
                    if( $row_count != -1)  
                    {
                        sqlsrv_rollback( $conn );
                        echo '<script>alert("Registro inexistente detectado hostname='.$dataCSV[0].' , rollback aplicado.");
                        window.location.href="cargar_CSV.php";
                        </script>';
                        die( print_r( sqlsrv_errors(), true) );
                    }
                    $flagnull=2;
                }
                else 
                {
                    echo '<script>alert("Registro vacio detectado, rollback aplicado.");
                        window.location.href="cargar_CSV.php";
                        </script>';
                    die( print_r( sqlsrv_errors(), true) );
                }
            }
            if($dataCSV[2]!=""||$dataCSV[3]!=""||$dataCSV[4]!="")
            {
                $flagOf2=1;
                $sql="update dbo.t_inv_host set ";
                for($j=2;$j<5;$j++)
                {
                    if($dataCSV[$j]!=''&&is_numeric($dataCSV[$j])==1)
                    {
                        if($flagOf2==1)
                        {
                            $sql .= $array[$j-2]." = ".$dataCSV[$j]." ";
                            $flagOf2=0;
                        }
                        else
                        $sql .= ", ".$array[$j-2]." = ".$dataCSV[$j]." ";
                    }
                }
                if($flagnull==1)
                    $sql .= "where ip='".$dataCSV[1]."'";
                if($flagnull==2)
                    $sql .= "where hostname='".$dataCSV[0]."'";
                $stmt = sqlsrv_query( $conn, $sql);
                if( $stmt === false) 
                {
                    sqlsrv_rollback( $conn );
                    echo '<script>alert("Error en insercion \n'.$sql.'\n, rollback aplicado.");
                    window.location.href="cargar_CSV.php";
                    </script>';
                    die( print_r( sqlsrv_errors(), true) );
                }
            }
        }
        sqlsrv_commit( $conn );
        echo '<script>alert("Archivo CSV cargado a la BD");
        window.location.href="cargar_CSV.php";
        </script>';
    }
    else
      echo '<script>alert("Archivo no existente o incompatible.");
      window.location.href="cargar_CSV.php";
      </script>';
?>