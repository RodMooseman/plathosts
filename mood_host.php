<?php
    include ('../bd/conexion_db.php');
    sqlsrv_begin_transaction( $conn );
    $today = date('d/m/Y');
    $array = array("id_tipo_host", "end_of_mark", "start_of_supp", "end_of_supp","fin_sop_hyp","id_tipo_hw",
    "serie","site","ubicacion","posicion","rack",
    "id_marca","modelo","hostname","ip","memoria",
    "cpu","cores_cpu","total_cores","id_tipo_proc",
    "procesador","sist_op","vcenter","cluster","id_ambiente",
    "proyecto","aplicacion","id_herr_monit","comentario","VMs","p_cpu","p_mem");
    $array2 = array("tipo", "end_of_mark", "start_of_supp", "end_of_supp","fin_sop_hyp","tipo_hw","serie","site","ubicacion","posicion","rack","marca","modelo","hostname","ip","memoria","cpu","cores_cpu","total_cores","tipo_proc","procesador","sist_op","vcenter","cluster","ambiente","proyecto","aplicacion","herr_monit","comentario","VMs","p_cpu","p_mem");
    $sql="select id_tipo_host,end_of_mark,start_of_supp,end_of_supp,fin_sop_hyp,id_tipo_hw,serie,
    site,ubicacion,posicion,rack,id_marca,modelo,
    hostname,ip,memoria,cpu,cores_cpu,total_cores,
    id_tipo_proc,procesador,sist_op,vcenter,cluster,id_ambiente,proyecto,
    aplicacion,id_herr_monit,comentario,VMs,p_cpu,p_mem,historia
    from dbo.t_inv_host";
    if($_POST['flagsome']=="ip")
        $sql .= " where ip='".$_POST['thisFlag']."'";
    else 
        $sql .= " where hostname='".$_POST['thisFlag']."'";
    $stmt = sqlsrv_query( $conn, $sql );
    $Dinfo=sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC);
    $sql="UPDATE dbo.t_inv_host SET ";
    $j=1;
    $dat="";
    for ($i=0;$i<sizeof($array);$i++)
    {
        if($_POST[$array2[$i]]!=$Dinfo[$array[$i]])
        {
            if($j==1)
            {
                if($i==0||$i==5||$i==11||($i<20&&$i>14)||$i==24||$i==27)
                {
                    $sql .= $array[$i]." = ".$_POST[$array2[$i]]." ";
                    $j=0;
                    $dat =$array2[$i];
                }
                else
                {
                    $sql .= $array[$i]." = '".$_POST[$array2[$i]]."' ";
                    $j=0;
                    $dat =$array2[$i];
                }
            }
            else
            {
                if($i==0||$i==5||$i==11||($i<20&&$i>14)||$i==24||$i==27)
                {
                    $sql .=" ,".$array[$i]." = ".$_POST[$array2[$i]]." ";
                    $dat .= ", ".$array2[$i];
                }
                else
                {
                    $sql .=" ,".$array[$i]." = '".$_POST[$array2[$i]]."' ";
                    $dat .= ", ".$array2[$i];
                }
            }
        }
    }  
    $hst="\n".$Dinfo['historia']." ".$dat." modificado el dia ".$today." por ".$_POST['user'];
    if($dat!="")
    {
        if($j==1)
            $sql .= "historia='".$hst."' ";
        else 
            $sql .= ", historia='".$hst."' ";
        if($_POST['flagsome']=="ip")
            $sql .= " where ip='".$_POST['thisFlag']."'";
        else 
            $sql .= " where hostname='".$_POST['thisFlag']."'";
        $stmt = sqlsrv_query( $conn, $sql );
        if( $stmt === false) 
        {
            sqlsrv_rollback( $conn );
            //echo $sql;
            echo '<script>alert("Error en actualizacion , rollback aplicado.");
                    window.location.href="modificar.php";
                </script>';
            die( print_r( sqlsrv_errors(), true) );
        }
        sqlsrv_commit( $conn );
        echo '<script>alert("Datos '.$dat.' modificados.");
            window.location.href="modificar.php";
        </script>';
    }
    else
        echo '<script>alert("Ningun dato ha cambiado."); window.location.href="modificar.php"; </script>';
?>