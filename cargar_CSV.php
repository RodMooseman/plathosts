<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Inventario Hosts</title>
        <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <style>
            .bd-placeholder-img {
                font-size: 1.125rem;
                text-anchor: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                user-select: none;
            }   
            @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
                }
            }
        </style>
        <link href="../bootstrap/css/sidebars.css" rel="stylesheet" type="text/css"/>
        <!--<script src="../jquery/jquery-3.6.0.min.js" type="text/javascript"></script>
        <script src="../js/filtra_querys.js" type="text/javascript"></script>
        <script src="../bootstrap/js/sidebars.js" type="text/javascript"></script>-->
        <script src="../bootstrap/js/bootstrap.bundle.js" type="text/javascript"></script>
    </head>
    <body>
        <?php 
            include('validaUsuario.php');
            $nombre_usu = $row['nombre_usu'];
            $id_perfil = $row['id_perfil'];
            $usu_univ = $row['usu_univ'];
        ?>  
        <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
            <symbol id="home" viewBox="0 0 16 16">
                <path d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146zM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5z"/>
            </symbol>
            <symbol id="gear-fill" viewBox="0 0 16 16">
                <path d="M9.405 1.05c-.413-1.4-2.397-1.4-2.81 0l-.1.34a1.464 1.464 0 0 1-2.105.872l-.31-.17c-1.283-.698-2.686.705-1.987 1.987l.169.311c.446.82.023 1.841-.872 2.105l-.34.1c-1.4.413-1.4 2.397 0 2.81l.34.1a1.464 1.464 0 0 1 .872 2.105l-.17.31c-.698 1.283.705 2.686 1.987 1.987l.311-.169a1.464 1.464 0 0 1 2.105.872l.1.34c.413 1.4 2.397 1.4 2.81 0l.1-.34a1.464 1.464 0 0 1 2.105-.872l.31.17c1.283.698 2.686-.705 1.987-1.987l-.169-.311a1.464 1.464 0 0 1 .872-2.105l.34-.1c1.4-.413 1.4-2.397 0-2.81l-.34-.1a1.464 1.464 0 0 1-.872-2.105l.17-.31c.698-1.283-.705-2.686-1.987-1.987l-.311.169a1.464 1.464 0 0 1-2.105-.872l-.1-.34zM8 10.93a2.929 2.929 0 1 1 0-5.86 2.929 2.929 0 0 1 0 5.858z"/>
            </symbol>
            <symbol id="people-circle" viewBox="0 0 16 16">
                <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
                <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
            </symbol>
        </svg>
        <main>
            <div class="d-flex flex-column flex-shrink-0 p-3 bg-light" style="width: 222px;" position: fixed;>
                <div class="py-1 text-center">
                    <h5>TD Infraestructura</h5>
                </div>
                <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto link-dark text-decoration-none">
                    <img src="../img/logotelcel.png" alt="" width="180" height="50"/>
                </a>  
                <hr>
                <ul class="nav nav-pills flex-column mb-auto">
                    <li class="nav-item">
                        <a href="principal.php" class="nav-link link-dark" title="Inicio" data-bs-toggle="tooltip" data-bs-placement="right">
                            <svg class="bi me-2" width="16" height="16"><use xlink:href="#home"/></svg>
                            Inicio
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="inventario.php" class="nav-link link-dark" title="Inventario" data-bs-toggle="tooltip" data-bs-placement="right">
                            <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                            Inventario
                        </a>
                    </li>
                    <li>
                        <a href="consultar.php" class="nav-link link-dark" title="Consultar" data-bs-toggle="tooltip" data-bs-placement="right">
                            <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                            Consultar
                        </a>
                    </li>
                    <li>
                        <a href="captura_hosts.php" class="nav-link link-dark" title="Dar de alta host." data-bs-toggle="tooltip" data-bs-placement="right">
                            <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                            Registrar Hosts
                        </a>
                    </li>
                    <li>
                        <a href="modificar.php" class="nav-link link-dark" title="Modificar host." data-bs-toggle="tooltip" data-bs-placement="right">
                            <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                            Modificar Host
                        </a>
                    </li>
                    <li>
                        <a href="cargar_CSV.php" class="nav-link active" aria-current="page">
                            <svg class="bi me-2" width="16" height="16"><use xlink:href="#gear-fill"/></svg>
                            Cargar CSV
                        </a>
                    </li>
                </ul>
                <hr>
                <div class="dropdown">
                    <a href="#" class="" id="dropdownUser2" data-bs-toggle="dropdown">
                        <svg class="bi me-2" width="16" height="16"><use xlink:href="#people-circle"/></svg>
                        <span class="text-justify"><?php echo $nombre_usu; ?></span>
                    </a>
                    <ul class="dropdown-menu text-small shadow" aria-labelledby="dropdownUser2">
                        <li><a class="dropdown-item" href="salir.php">Cerrar Sesión</a></li>
                    </ul>
                </div>
            </div>
            <div class="b-example-divider"></div>
            <section class="py-5 text-center container">
                <div class="row py-lg-5">
                    <div class="col-lg-6 col-md-8 mx-auto">
                        <h2 class="fw-light">Carga de archivo CSV</h2>
                        <form action='CSVchooseCH.php' method='post' enctype='multipart/form-data'><br>
                            <table>
                                <tr>
                                    <th>Tipo de <br>carga</th>
                                    <th>Seleccionar archivo:</th>
                                    <th><input type="hidden" id="nombre_usu" name="nombre_usu" value="<?php echo $nombre_usu;?>"></th>
                                </tr>
                                <tr>
                                    <td>
                                        <select name="tipoOfData" id="tipoOfData" onchange="typeOfHost(this)">
                                            <option value=""  ></option>
                                            <option value="1"  >CloudHost</option>
                                            <option value="2"  >Bare Metal</option>
                                        </select>
                                    </td>
                                    <td><input type='file' name='somefile' size='20' ></td>
                                    <td><input type='submit' name='thisGo' id='thisGo' disabled="disabled" value='Subir archivo'></td>
                                </tr>
                            </table>
                        </form>
                        <form action='CSVchooseVM.php' method='post' enctype='multipart/form-data'><br>
                            <table>
                                <tr>
                                    <!--<th>Tipo de <br>carga</th>-->
                                    <th>Seleccionar archivo VMs:</th>
                                    <th><input type="hidden" id="nombre_usu" name="nombre_usu" value="<?php echo $nombre_usu;?>"></th>
                                </tr>
                                <tr>
                                    <!--<td>
                                        <select name="tipoOfData" id="tipoOfData" onchange="typeOfHost(this)">
                                            <option value=""  ></option>
                                            <option value="1"  >CloudHost</option>
                                            <option value="2"  >Bare Metal</option>
                                        </select>
                                    </td>-->
                                    <td><input type='file' name='somefile' size='20' ></td>
                                    <td><input type='submit' value='Subir archivo'></td>
                                </tr>
                            </table>
                        </form>
                    </div>
                </div>
            </section>
        </main>
    </body>
    <script>
        function typeOfHost(select)
        {
            if(select.value=="")
            document.getElementById('thisGo').disabled = true;
            else
            document.getElementById('thisGo').disabled = false;
        }
    </script>
</html>